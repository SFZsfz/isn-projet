import pygame

pygame.init()
from player import Player
from floor import Floor

win = pygame.display.set_mode((1024, 768))
pygame.display.set_caption("Ninja Dodge")
# pygame.display.set_icon("Kunai.ico")

walkRight = [pygame.image.load('./images/R1.png'), pygame.image.load('./images/R2.png'),
             pygame.image.load('./images/R3.png'), pygame.image.load('./images/R4.png'),
             pygame.image.load('./images/R5.png'), pygame.image.load('./images/R6.png'),
             pygame.image.load('./images/R7.png'), pygame.image.load('./images/R8.png'),
             pygame.image.load('./images/R9.png')]
fond = pygame.image.load("./images/asset/BG.jpg")
floor = pygame.image.load("./images/asset/floor.jpg").convert_alpha()
base = pygame.image.load("./images/I0.png")

clock = pygame.time.Clock()

player = Player()
sol = Floor(270, 630)
sol2 = Floor(100, 500)


run = True

while run:

    win.blit(fond, (0, 0))

    win.blit(sol.image, sol.rect)
    win.blit(sol2.image, sol2.rect)

    win.blit(player.image, player.rect)

    pygame.display.update()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        elif event.type == pygame.KEYDOWN:

            keys = pygame.key.get_pressed()

            if event.key == pygame.K_RIGHT:
                player.move_d()

            elif event.key == pygame.K_LEFT:
                player.move_g()
            elif event.key == pygame.K_SPACE:
                print('saut')

pygame.quit()

